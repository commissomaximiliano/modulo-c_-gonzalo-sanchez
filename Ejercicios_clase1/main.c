#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_STRING 80
//Guia Practica 1
void ejercicio1(int punto)
{
    int i = 102;
    int j = -56;
    long ix = -158931574;
    unsigned u = 35460;
    float x = 12.687;
    double dx = 0.000000025;
    char c = 'C';

    switch(punto)
    {

    case 1:
        printf("%04d , %04d , %14.8e, %14.8e \n",i,j,x,dx );
        break;

    case 2:
        printf("%05d , %05d, %012l , %010.5f, %05d \n",i,j,ix,x,u );
        break;
    case 3:
        printf("%05d , %05d, %012d \n \n %10.5f, %05d \n",i,j,ix,x,u );
        break;
    case 4:
        printf("%06d\n\n\n%06d\n\n\n%06d\n\n\n",i,u,c);
        break;
    case 5:
        printf("%05d %05d %11.4f \n",u,j,x);
        break;
    case 6:
        printf("%-5d %-5d %-11.4f \n",u,j,x);
        break;
    case 7:
        printf("%+05d %+05d %+11.4f \n",u,j,x);
        break;
    case 8:
        printf("%+05d %+05d %+011.4f \n",u,j,x);
        break;
    case 9:
        printf("%+05d %+05d %+011.1f \n",u,j,x);
        break;
    }
}

void ejercicio2()
{
    char nombre[30];
    float x1;
    float x2;
    printf("Por favor ingresa tu nombre:\n");
    scanf("%s",nombre);

    printf("%s\n",nombre);
    printf("ingrese x1 \n");
    scanf("%f",&x1);

    printf("ingrese x2 \n");
    scanf("%f",&x2);

    printf("x1= %.1f  x2= %.1f\n",x1,x2);




}



void ejercicio3()
{

    int a,b;

    printf("ingrese un valor:\n");
    scanf("%d",&a);

    printf("ingrese otro valor:\n");
    scanf("%d",&b);

    printf("La suma es: %-+d \n", a+b);








}


void control_ej1()
{
    char value[MAX_STRING];
    printf("ingrese una palabra \n");
    scanf("%s",value);
    int cant_string=strlen(value);
    if(cant_string<=MAX_STRING)
    {
        char value_inv[cant_string];
        for (int i=0; i<cant_string; i++)
        {
            value_inv[cant_string-1-i]=value[i];


        }
        printf("%s\n",value_inv);

    }
    else
    {

        printf("Error: Superado la cantidad maxima de caracteres\n");
    }
}

float potencia(float value,int pot){
float acum=1;
for (int i=0; i<pot ; i++){
    acum*=value;

}

return acum;

}
float control_ej2(float dinero, float interes , int tiempo ){

float suma=0;

    for(int i=0 ; i<tiempo ; i++){

    suma+=potencia(1+ (interes/100.0),i+1);



    }


    return suma*dinero;




}


// Guia 1:
int main()
{

    printf("Ejercicio 1: \n");
    printf("A,B \n");
    ejercicio1(1);
    printf("C \n");
    ejercicio1(2);
    printf("D \n");
    ejercicio1(3);
    printf("E \n");
    ejercicio1(4);
    printf("F \n");
    ejercicio1(5);
    printf("G \n");
    ejercicio1(6);
    printf("H \n");
    ejercicio1(7);
    printf("I \n");
    ejercicio1(8);
    printf("J \n");
    ejercicio1(9);

    printf("Ejercicio 2: \n");
    ejercicio2();

    printf("Ejercicio 3: \n");
    ejercicio3();

    printf("Guia I: Instrucciones de control\n");
    printf("Ejercicio1\n");
    control_ej1();
    printf("Ejercicio2\n");
    printf("F= %.4f\n",control_ej2(100,6,30));
    printf("F= %.4f\n",control_ej2(100000,6,30));




    return 0;
}
