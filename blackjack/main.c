#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#define NUM_CARTAS 52 // Cartas totales del maso
#define ESPECIALES 12 // ESPECIALES en el maso J, Q ,K
#define MAX_POINTS 21
#define INICIALES 4 // Cartas obligatorias para ambos jugadores 2 c/u

enum figura
{
    CORAZONES_ROJOS  = 501,
    PICAS,
    CORAZONES_NEGROS,
    TREBOL

};

enum especiales
{
    N=70, //Normal
    J,  //CARTA J
    Q,
    K

};

typedef struct
{

    int value;
    enum figura figure;
    enum especiales especial;



} carta;

int puntos(carta *carta_value, int dim)
{

    int suma=0;
    int suma_as=0;


    for(int i=0 ; i<dim ; i++)
    {


        suma+=carta_value[i].value;
        suma_as+=carta_value[i].value;

    }

    if(suma<MAX_POINTS)
    {

        for(int i=0 ; i<dim ; i++)
        {
            if(carta_value[i].value==1)
            {

                suma_as+=10;


            }


        }

    }

    if(suma_as==MAX_POINTS)
    {
        return suma_as;
    }
    else
    {
        return suma;


    }
}


bool confirmar_ocupado(int valor,int *ocupados,int dim)
{

    int i=0;
    bool ocupado=false;
    while(i<dim && !ocupado)
    {

        if(ocupados[i]==valor)
        {

            ocupado=true;
        }


        i++;
    }

    return ocupado;




}

void cargar_maso(carta *maso)
{
    int j=0;
    int k=0;
    for(int i=0 ; i<NUM_CARTAS-ESPECIALES ; i++)
    {

        maso[i].value=j+1;
        maso[i].figure=CORAZONES_ROJOS+k;
        maso[i].especial=N;
        j++;
        if(j>(NUM_CARTAS-ESPECIALES)/4-1)
        {

            j=0;
            k++;

        }


    }
    j=0;
    k=1;
    for(int i=NUM_CARTAS-ESPECIALES ; i<NUM_CARTAS; i++)
    {


        maso[i].value=10;
        maso[i].figure=CORAZONES_ROJOS+j;
        maso[i].especial=N+k;
        j++;
        if(j>ESPECIALES/4)
        {

            j=0;
            k++;

        }





    }




}

void print_maso(carta *maso)
{

    for(int i=0; i<NUM_CARTAS; i++)
    {

        printf("Valor : %d , Figura: %d , Tipo: %d \n", maso[i].value,maso[i].figure, maso[i].especial);


    }



}


int main()
{
    time_t t;

    carta maso[NUM_CARTAS];
    int ocupadas[NUM_CARTAS]= {-1};
    carta j1[NUM_CARTAS];
    carta j2[NUM_CARTAS];
    bool end=false;
    int mano=0;
    int index_maso=-1;
    int cant_j1=0;
    int cant_j2=0;
    int puntos_j1=0;
    int puntos_j2=0;
    int num_ocupados=0;
    cargar_maso(maso);
    print_maso(maso);
    char respuesta[2];
    bool j1_resp=false;
    bool j2_resp=false;

    while(!end && num_ocupados<NUM_CARTAS && puntos_j1<MAX_POINTS && puntos_j2<MAX_POINTS)
    {
        srand(time(&t));
        index_maso= rand()%NUM_CARTAS;
        if(confirmar_ocupado(index_maso,ocupadas,NUM_CARTAS)==false)
        {
            if(mano>INICIALES-1)
            {
                if(mano % 2 == 0)
                {
                    if(j1_resp==false)
                    {
                        printf("Jugador 1 se planta o sigue? si o no\n");
                        scanf("%s",respuesta);
                        fflush(stdin);
                        if(strncmp(respuesta,"si",2)==0)
                        {
                            j1_resp=true;
                        }
                        else
                        {
                            j1_resp=false;
                        }



                    }
                }
                else
                {
                    if(j2_resp==false)
                    {
                        printf("Jugador 2 se planta o sigue? si o no\n");
                        scanf("%s",respuesta);
                        fflush(stdin);
                        if(strncmp(respuesta,"si",2)==0)
                        {
                            j2_resp=true;
                        }
                        else
                        {
                            j2_resp=false;
                        }
                    }

                }


            }



            if(mano % 2 == 0)
            {
                if(j1_resp==false)
                {
                    j1[cant_j1]=maso[index_maso];

                    cant_j1++;
                    ocupadas[num_ocupados]=index_maso;
                    num_ocupados++;
                    puntos_j1=puntos(j1,cant_j1);



                }
                mano++;

            }
            else
            {
                if(j2_resp==false)
                {
                    j2[cant_j2]=maso[index_maso];

                    cant_j2++;
                    ocupadas[num_ocupados]=index_maso;
                    num_ocupados++;
                    puntos_j2=puntos(j2,cant_j2);

                }
                mano++;
            }




            if(j2_resp==true && j1_resp==true)
            {

                end=true;
            }




        }

    }


    printf("%d , %d\n",puntos_j1,puntos_j2);
    if(puntos_j1 > puntos_j2)
    {


        if(puntos_j1==MAX_POINTS)
        {

            printf("Jugador 1 Ganador\n");
        }
        else
        {
            if(puntos_j1> MAX_POINTS)
            {
                printf("Jugador 2 Ganador\n");
            }
            else
            {
                printf("Jugador 1 Ganador\n");
            }
        }
    }
    else
    {

        if(puntos_j1 <  puntos_j2)
        {

            if(puntos_j2==MAX_POINTS)
            {

                printf("Jugador 2 Ganador\n");
            }
            else
            {

                if(puntos_j2> MAX_POINTS)
                {
                    printf("Jugador 1 Ganador\n");
                }
                else
                {
                    printf("Jugador 2 Ganador\n");
                }
            }



        }
        else
        {

            printf("Empate: Ninguno gana ");


        }






    }

    return 0;
}

