#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define TAM 10
void ordenamiento_burbuja_valorABS(float *lista, int tam)
{
    float temp=0;
    for (int i=1; i<tam; i++)
    {
        for (int j=0 ; j<tam - 1; j++)
        {
            if (fabs(lista[j]) > fabs(lista[j+1]))
            {
                temp = lista[j];
                lista[j] = lista[j+1];
                lista[j+1] = temp;


            }


        }

    }


}

void ordenamiento_burbuja_valorABS_desc(float *lista, int tam)
{
    float temp=0;
    for (int i=1; i<tam; i++)
    {
        for (int j=0 ; j<tam - 1; j++)
        {
            if (fabs(lista[j]) < fabs(lista[j+1]))
            {
                temp = lista[j];
                lista[j] = lista[j+1];
                lista[j+1] = temp;


            }


        }

    }


}

void ordenamiento_burbuja_algebraico(float *lista, int tam)
{
    float temp=0;
    for (int i=1; i<tam; i++)
    {
        for (int j=0 ; j<tam - 1; j++)
        {
            if (lista[j] > lista[j+1])
            {
                temp = lista[j];
                lista[j] = lista[j+1];
                lista[j+1] = temp;


            }


        }

    }


}

void ordenamiento_burbuja_algebraico_desc(float *lista, int tam)
{
    float temp=0;
    for (int i=1; i<tam; i++)
    {
        for (int j=0 ; j<tam - 1; j++)
        {
            if (lista[j] < lista[j+1])
            {
                temp = lista[j];
                lista[j] = lista[j+1];
                lista[j+1] = temp;


            }


        }

    }


}
int main()
{
    float arreglo[TAM]= {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
    int opcion=0;
    printf("ingrese una opcion\n");
    printf("1-Ordenamiento de menor a mayor en valor absoluto\n");
    printf("2-Ordenamiento de menor a mayor algebraicamente (con signo).\n");
    printf("3-Ordenamiento de mayor a menor en valor absoluto.\n");
    printf("4-Ordenamiento de mayor a menor algebraicamente (con signo).\n");
    scanf("%d",&opcion);
    switch(opcion)
    {
    case 1:
        ordenamiento_burbuja_valorABS(arreglo,TAM);
        break;
    case 2:
        ordenamiento_burbuja_algebraico(arreglo, TAM);
        break;
    case 3:
        ordenamiento_burbuja_valorABS_desc(arreglo,TAM);
        break;
    case 4:
        ordenamiento_burbuja_algebraico_desc(arreglo,TAM);
        break;
    default:
        ordenamiento_burbuja_algebraico(arreglo, TAM);
        break;

    }

    for (int i=0; i<TAM ; i++)
    {
        printf("%.1f , ", arreglo[i]);
    }

    return 0;
}
